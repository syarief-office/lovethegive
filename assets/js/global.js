/* Author:http://www.rainatspace.com

*/

function initializeScript(){
	//jQuery('#preloader').fadeOut(2000);
		jQuery("[icon-data]").each(function(){
		var getAttr =  jQuery(this).attr('icon-data');
		jQuery(this).addClass(getAttr).removeAttr('icon-data');
	});

	// Navigation  Bounce Menu Effect================================================================*/
	jQuery('.nav li ul').removeClass('hidden');
		jQuery('.nav li').hover(function() {
			jQuery('ul', this).filter(':not(:animated)').slideDown(600, 'easeOutBounce');
	     }, function() {
		jQuery('ul', this).slideUp(600, 'easeInExpo');
	});

	// NAVIGATION RESPOSNIVE HANDLER
	jQuery(".nav >  ul").clone(false).appendTo(".nav-rwd-sidebar");
	jQuery(window).on('load', function(){
		jQuery('.nav-rwd-sidebar').find('ul').removeClass();
	});
	jQuery(".btn-rwd-sidebar, .btn-hide").click( function() {
		jQuery(".nav-rwd-sidebar").toggleClass("sidebar-active");
		jQuery(".wrapper-inner").toggleClass("wrapper-active");
	});


	//TABS
	jQuery("#tabs li").click(function() {
		jQuery("#tabs li").removeClass('active');
		jQuery(this).addClass("active");
		jQuery(".tab_content").hide();
		var selected_tab = jQuery(this).find("a").attr("href");
		jQuery(selected_tab).fadeIn();
		return false;
	});


	//FIXED NAV
	jQuery(window).scroll(function() {
		if (jQuery(this).scrollTop() > 1){  
	        jQuery('#header').addClass("sticky");
	    }
	    else{
	        jQuery('#header').removeClass("sticky");
	    }
	});

	
	//SMOOTHSCROLL
	jQuery('.scrollTargeted').click(function() {
	    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
	      var target = jQuery(this.hash);
	      target = target.length ? target : jQuery('[name=' + this.hash.slice(1) +']');
	      if (target.length) {
	        jQuery('html,body').stop().animate({
	          scrollTop: target.offset().top
	        }, 1000);
	        return false;
	      }
	    }
  	});

  	//SCROLL TOP
  	jQuery(window).scroll(function() {
  		if(jQuery(this).scrollTop() >  700){
  			jQuery('#scrollToTop').fadeIn();
  		}else{
  			jQuery('#scrollToTop').fadeOut();
  		}
  	});
  	jQuery('#scrollToTop').on("click", function(){
  		jQuery("html, body").animate({
  			scrollTop : 0
  		},800);
  		return false;
  	});
}

/* =Document Ready Trigger
-------------------------------------------------------------- */
jQuery(document).ready(function(){
    initializeScript();
    //FREEWALL - GRID LAYOUT
	var wall = new freewall(".freewall");
		wall.reset({
		selector: '.tumblrnews-item',
		//draggable: true,
		animate: true,
		cellW: 210,
		cellH: 'auto',
		onResize: function() {
			wall.fitWidth();
		}, 
		gutterX: 0,
		gutterY: 0
		//bottomToTop: true,
	});
	wall.fitWidth();
	// for scroll bar appear;
	jQuery(window).trigger("load, resize");
	jQuery(".fancybox").fancybox({
		maxWidth	: 675,
		//maxHeight	: 415,
		fitToView	: false,
		openEffect	: 'fade',
		closeEffect	: 'fade'
		//autoSize	: false

	});
});
/* END ------------------------------------------------------- */